// Jimmy Truong | December 9 2020 | pipeline for sample_code2

pipeline {
   agent any
   parameters {
       booleanParam(defaultValue: false, description: 'Run Unit Tests', name: 'TEST')
   }
   stages {
       stage('Student ID and Group Number') {
           steps {
               echo "A00886105, Group 27"
           }
       }

       stage('Build') {
           steps {
               echo 'Installing the Python Requirements...'
               sh 'pip install -r requirements.txt'
               echo 'Requirement complete.'
           }
       }

       stage('Code Quality') {
           steps {
               sh 'pylint-fail-under --fail_under 5.0 *.py'
           }
       }

       stage('Code Quantity') {
           steps {
               sh 'ls -1q *.py | wc -l'
           }
       }

       stage('Test') {
           when {
               expression { params.TEST }
           }
           steps {
               sh "coverage run --omit */dist-packages/*,*/site-packages/* test_book_manager.py"
           }
           post {
               always {
                   script {                
                       def test_reports_exist = fileExists '/test-reports'

                       if (test_reports_exist) {              
                           junit 'test-reports/*.xml'                  
                       }

                       def api_test_reports_exist = fileExists '/api-test-reports'

                       if (api_test_reports_exist) {          
                           junit 'api-test-reports/*.xml'                
                       }
                   }
               }
           }
       }

       stage('Package') {
           steps {
               sh "zip app.zip *.py"
               archiveArtifacts artifacts: 'app.zip', fingerprint: true
           }
       }
   }
}
